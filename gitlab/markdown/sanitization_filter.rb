# allow script tag and src attribute
whitelist[:remove_contents].delete('script')
whitelist[:elements].push('script')
whitelist[:attributes]['script'] = %w(src)